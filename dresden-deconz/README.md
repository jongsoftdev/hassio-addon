# Raspbee deCONZ

![Docker Image Version (latest semver)](https://img.shields.io/docker/v/gjong/armhf-deconz-addon?sort=semver)
![Layers](https://img.shields.io/microbadger/image-size/gjong/armhf-deconz-addon.svg)
![status](https://img.shields.io/badge/status-ready-green.svg)

The Raspbee is a RasberryPi shield that enables the controlling of Zigbee devices using
the deCONZ software.

This docker images is designed to be run on any Hass.io running on a RasberryPi with a Raspbee
shield installed. 

> Brings ZigBee connectivity to Raspberry Pi. The shield allows direct communication with ZigBee PRO devices like Philips Hue, XBee Series 2 and others.

## Adding devices
After the installation and starting of the addon you can use the Phoscon inferface to add and manage connected
zigbee devices.

* Wireless Light Control (deprecated): http://hassio.local:8080 
* Phoscon: http://hassio.local:8080/pwa 

The default username/password is delight/delight.

## Preparing the RaspberyPi

### Enable the UART 

* Shutdown the PI
* Remove the SD Card and insert in a PC
* Open the config.txt in the drive that automatically mounts
* Add the following lines at the bottom of the file

```bash
enable_uart=1
dtoverlay=pi3-disable-btroot
```

Some devices require an alternate configuration:

```bash
dtoverlay=pi3-disable-bt
```

### Updating the firmware
Automated updating of the firmware is currently not working in the addon, but can be done manually. To do this complete the following
steps:

* Stop the addon using the Hassio interface
* Login to the [Hass.io SSH](https://home-assistant.io/developers/hassio/debugging/) (this should be on port 2222 with certificate login only)
* Run the following command:
```bash
docker run --rm -it --entrypoint "/bin/bash" --privileged \ 
       --device /dev/ttyAMA0:/dev/ttyAMA0:rwm \ 
       gjong/armhf-deconz-addon GCFFlasher_internal -f \ 
       /usr/share/deCONZ/firmware/deCONZ_Rpi_0x262f0500.bin.GCF
```
* Start the addon using the Hassio interface 

This will start an upgrade of the firmware on the Raspbee

## Setting up with Home Assistant

If `discovery:` is enabled in the configuration.yaml, navigate to http://hassio.local:8123 and select the `Configure deCONZ` 
option that should be displayed

If `discovery:` is not enabled, or you wish to configure it manually add the following code to the configuration.yaml:

```yaml
deconz:
  host: hassio.local
# Change the port below to the one choosen in the addon
  port: 8080
```

Read more on the deCONZ setup on [deCONZ component](http://home-assistant.io/components/deconz) page

## Things to keep in mind

* The firmware upgrades do not work inside the container
* OTA updates are not yet confirmed to work from within the container

## Configuration of the addon
**Note:** Remember to restart the add-on when the configuration is changed.

```yaml
{
    "deconz_device": "/dev/ttyAMA0",
    "info": 1,
    "aps": 0,
    "zcl": 0,
    "zdp": 0,
    "otau": 0,
    "vnc_active": false,
    "upgrade_firmware": false
  }
```

### Option: `deconz_device`
By default, deCONZ searches for RaspBee at /dev/ttyAMA0 and Conbee at /dev/ttyUSB0; when using other USB devices (e.g. a Z-Wave stick)
deCONZ may not find RaspBee/Conbee properly. Set this environment variable to the same string passed to --device to force deCONZ to use the specific USB device.

### Option: `info`
Sets the level of the deCONZ command-line flag --dbg-info (default 1).

### Option: `aps`
Sets the level of the deCONZ command-line flag --dbg-aps (default 0).

### Option: `zcl`
Sets the level of the deCONZ command-line flag --dbg-zcl (default 0).

### Option: `zdp`
Sets the level of the deCONZ command-line flag --dbg-zdp (default 0).

### Option: `otau`
Sets the level of the deCONZ command-line flag --dbg-otau (default 0).

### Option: `vnc_active`
Indicates if a TigerVNC server should be started with a virtual display. This allows for connecting to the deCONZ software using a vncviewer application.

### Option: `vnc_password`
The password that should be set when starting the TigerVNC server.

**Note:** this option should be used when `vnc_active` is set to `true`.

## License
MIT License

Copyright (c) 2019 Gerben Jongerius

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
