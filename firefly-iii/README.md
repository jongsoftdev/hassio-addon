# Firefly-III Hassio addon

![Image version](https://images.microbadger.com/badges/version/gjong/armhf-firefly-iii-addon.svg)
![Layers](https://img.shields.io/microbadger/image-size/gjong/armhf-firefly-iii-addon.svg)
![Status](https://img.shields.io/badge/status-alpha-orange.svg)

"Firefly III" is a (self-hosted) manager for your personal finances. It can help you keep track of your expenses and income, so you can spend less and save more. Firefly III supports the use of budgets, categories and tags. 
It can import data from external sources and it has many neat financial reports available. 

[![The index of Firefly III](https://firefly-iii.org/static/screenshots/4.7.4/tiny/index.png)](https://firefly-iii.org/static/screenshots/4.7.4/index.png) [![The account overview of Firefly III](https://firefly-iii.org/static/screenshots/4.7.4/tiny/account.png)](https://firefly-iii.org/static/screenshots/4.7.4/account.png)
[![Overview of all budgets](https://firefly-iii.org/static/screenshots/4.7.4/tiny/budget.png)](https://firefly-iii.org/static/screenshots/4.7.4/budget.png) [![Overview of a category](https://firefly-iii.org/static/screenshots/4.7.4/tiny/category.png)](https://firefly-iii.org/static/screenshots/4.7.4/category.png)

## About
Personal financial management is pretty difficult, and everybody has their own approach to it. Some people make budgets, other people limit their cashflow by throwing away their credit cards, others try to increase their current cashflow. There are tons of ways to save and earn money. Firefly III works on the principle that if you know where you're money is going, you can stop it from going there.

By keeping track of your expenses and your income you can budget accordingly and save money. Stop living from paycheck to paycheck but give yourself the financial wiggle room you need.

You can read more about this in the [official documentation](https://firefly-iii.readthedocs.io/en/latest/index.html).

## Configuration
**Note:** Remember to restart the add-on when the configuration is changed.

```yaml
{
    "app_env": "local",
    "app_key": "ipfvvhitpdrebwtrvwdtszppgzckphgt",
    "db_type": "sqlite"
}
```

### Option: app_url
The `app_url` of the application, this can be used to setup an alternative pretty URL rather then the IP address 

### Option: timezone
The `timezone` of the application. 

### Option: db_type
The `db_type` indicates what type of database is used to store the data. Possible values are:

* mysql
* sqlite

### Option: db_server
The `db_server` contains the DNS name or IP address of the MySQL server. 

**Note:** This option is required when setting `db_type` to `mysql` 

### Option: db_port
The `db_port` contains the port of the MySQL server. 

**Note:** This option is required when setting `db_type` to `mysql` 

### Option: db_schema
The `db_schema` contains the schema name to be used inside the MySQL server. 

**Note:** This option is required when setting `db_type` to `mysql`

### Option: db_user
The `db_user` contains the username to use when connecting to the MySQL server. 

**Note:** This option is required when setting `db_type` to `mysql`

### Option: db_password
The `db_password` contains the password to use when connecting to the MySQL server. 

**Note:** This option is required when setting `db_type` to `mysql`

## License
MIT License

Copyright (c) 2019 Gerben Jongerius

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
