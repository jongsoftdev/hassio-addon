#!/usr/bin/env bash
CONFIG_PATH=/data/options.json

HASSIO_DEBUG_INFO="$(jq --raw-output '.info' ${CONFIG_PATH})"
HASSIO_DEBUG_APS="$(jq --raw-output '.aps' ${CONFIG_PATH})"
HASSIO_DEBUG_ZCL="$(jq --raw-output '.zcl' ${CONFIG_PATH})"
HASSIO_DEBUG_ZDP="$(jq --raw-output '.zdp' ${CONFIG_PATH})"
HASSIO_DEBUG_OTAU="$(jq --raw-output '.otau' ${CONFIG_PATH})"
HASSIO_DEVICE_PATH="$(jq --raw-output '.deconz_device' ${CONFIG_PATH})"
HASSIO_VNC_MODE="$(jq --raw-output '.vnc_active' ${CONFIG_PATH})"

echo "[Hass.io] Current deCONZ version: ${DECONZ_VERSION}"
echo "[Hass.io] Device: $HASSIO_DEVICE_PATH"

DECONZ_OPTIONS="--auto-connect=1 \
                  --dbg-info=$HASSIO_DEBUG_INFO \
                  --dbg-aps=$HASSIO_DEBUG_APS \
                  --dbg-zcl=$HASSIO_DEBUG_ZCL \
                  --dbg-zdp=$HASSIO_DEBUG_ZDP \
                  --dbg-otau=$HASSIO_DEBUG_OTAU \
                  --http-port=8080 \
                  --ws-port=8843"

if [ "${HASSIO_VNC_MODE}" != "true" ]; then
    DECONZ_OPTIONS="${DECONZ_OPTIONS} -platform minimal"
fi

DECONZ_OPTIONS="${DECONZ_OPTIONS} --dev=${HASSIO_DEVICE_PATH}"

export USER=root
export DISPLAY=:1

/usr/bin/deCONZ ${DECONZ_OPTIONS}
